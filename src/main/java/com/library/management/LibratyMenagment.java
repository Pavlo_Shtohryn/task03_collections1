package com.library.management;

import com.library.entity.Author;
import com.library.entity.Book;
import com.library.entity.Items;

import java.util.*;

import static java.lang.System.out;

public class LibratyMenagment implements Menegment {
    Scanner s = new Scanner(System.in);
    Set<Items> itemslSet = new LinkedHashSet<>();

    public Set<Items> getItems() {
        return itemslSet;
    }

    public void setItems(Set<Items> items) {
        this.itemslSet = items;
    }

    @Override
    public void addAuthor() {
        out.println("Adding an Author");
        out.print("Name: ");
        String name = s.nextLine();
        out.print("Surname: ");
        String surname = s.nextLine();
        itemslSet.add(new Items(new Author(surname, name)));
    }

    @Override
    public void addBook() {
        out.println("Enter the book's author");
        out.print("Name- ");
        String name = s.nextLine();
        out.print("Surname- ");
        String surname = s.nextLine();
        Iterator<Items> iterator = itemslSet.iterator();
        while (iterator.hasNext()) {
            Items items = (Items) iterator.next();
            if (name.equalsIgnoreCase(items.getAuthor().getName()) &&
                    surname.equalsIgnoreCase(items.getAuthor().getSurname())) {
                out.println("Add book: ");
                out.println("Title: ");
                String title = s.nextLine();
                out.println("Year: ");
                int year = s.nextInt();
                out.println("Pages: ");
                int pages = s.nextInt();
                items.getBooks().add(new Book(title, year, pages));
            }
        }
    }

    @Override
    public void removeAuthor() {
        out.println("Enter the Author: ");
        out.println("Name- ");
        String name = s.nextLine();
        out.println("Surname-");
        String surname = s.nextLine();
        Iterator<Items> iterator = itemslSet.iterator();
        while (iterator.hasNext()) {
            Items items = (Items) iterator.next();
            if (name.equalsIgnoreCase(items.getAuthor().getName()) &&
                    surname.equalsIgnoreCase(items.getAuthor().getSurname())) {
                iterator.remove();
            }
        }
    }

    @Override
    public void removeBook() {
        out.println("Removing book:");
        out.println("First of full enter the book's author ");
        out.print("Surname- ");
        String surname = s.nextLine();
        out.print("Surname- ");
        String name = s.nextLine();
        Iterator<Items> iterator = itemslSet.iterator();
        while (iterator.hasNext()) {
            Items items = (Items) iterator.next();
            if (name.equalsIgnoreCase(items.getAuthor().getName()) &&
                    surname.equalsIgnoreCase(items.getAuthor().getSurname())) {
                out.print("Enter book's title-");
                String title = s.nextLine();
                Iterator<Book> bookIterator = items.getBooks().iterator();
                while (bookIterator.hasNext()) {
                    Book book = (Book) bookIterator.next();
                    if (title.equalsIgnoreCase(book.getTitle())) {
                        bookIterator.remove();
                    }
                }
            }
        }
    }

    @Override
    public void removeAuthorsBooks() {
        out.print("Removing all author books");
        out.print("Enter the author:");
        out.print("Surname- ");
        String surname = s.nextLine();
        out.print("Name- ");
        String name = s.nextLine();
        Iterator<Items> iterator = itemslSet.iterator();
        Items items = (Items) iterator.next();
        if (name.equalsIgnoreCase(items.getAuthor().getName()) &&
                surname.equalsIgnoreCase(items.getAuthor().getSurname())) {
            items.getBooks().clear();
        }
    }

    @Override
    public void sortBooksByPages() {
        out.println("Sorting by book's size(number of pages)");
        out.println("Enter the author of books: ");
        out.print("Surname- ");
        String surname = s.nextLine();
        out.print("Name- ");
        String name = s.nextLine();
        Iterator<Items> iterator = itemslSet.iterator();
        Comparator<Book> bookComparator = new SortBooksByPages();
        Set<Book> books = new TreeSet<>(bookComparator);
        while (iterator.hasNext()) {
            Items items = (Items) iterator.next();
            if (name.equalsIgnoreCase(items.getAuthor().getName()) &&
                    surname.equalsIgnoreCase(items.getAuthor().getSurname())) {
                books.addAll(items.getBooks());
            }
        }
        for (Book i : books) {
            System.out.println(i);
        }
    }

    @Override
    public void sortBooksByYear() {
        out.println("Sorting by year: ");
        out.println("Enter the author of books: ");
        out.print("Name- ");
        String name = s.next();
        out.print("Surname- ");
        String surname = s.next();
        Iterator<Items> iterator = itemslSet.iterator();
        Comparator<Book> bookComparator = new SortBooksByYear();
        Set<Book> books = new TreeSet<>(bookComparator);
        while (iterator.hasNext()) {
            Items items = (Items) iterator.next();
            if (name.equalsIgnoreCase(items.getAuthor().getName()) &&
                    surname.equalsIgnoreCase(items.getAuthor().getSurname())) {
                books.addAll(items.getBooks());
            }
        }
        for (Book i : books) {
            System.out.println(i);
        }
    }
}
