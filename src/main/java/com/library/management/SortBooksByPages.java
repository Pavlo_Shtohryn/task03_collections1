package com.library.management;

import com.library.entity.Book;

import java.util.Comparator;

public class SortBooksByPages implements Comparator<Book> {
    @Override
    public int compare(Book o1, Book o2) {
        if (o1.getPages() == o2.getPages())
            return 0;
        else if (o1.getPages() > o2.getPages())
            return 1;
        else return -1;
    }
}
