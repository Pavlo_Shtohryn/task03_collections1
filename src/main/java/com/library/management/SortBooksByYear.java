package com.library.management;

import com.library.entity.Book;

import java.util.Comparator;

public class SortBooksByYear implements Comparator<Book> {
    @Override
    public int compare(Book o1, Book o2) {
        if (o1.getYear() == o2.getYear())
            return 0;
        else if (o1.getYear() > o2.getYear())
            return 1;
        else return -1;
    }
}
