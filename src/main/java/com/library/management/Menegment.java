package com.library.management;

public interface Menegment {
    void addAuthor();

    void addBook();

    void removeAuthor();

    void removeBook();

    void removeAuthorsBooks();

    void sortBooksByPages();

    void sortBooksByYear();

}
