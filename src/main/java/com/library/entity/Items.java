package com.library.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Items {
    private Author author;
    List<Book> books=new ArrayList<>();

    public Items(Author author, List<Book> books) {
        super();
        this.author = author;
        this.books = books;
    }

    @Override
    public String toString() {
        return "Items{" +
                "author=" + author +
                ", books=" + books +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Items items = (Items) o;
        return Objects.equals(author, items.author) &&
                Objects.equals(books, items.books);
    }

    @Override
    public int hashCode() {
        return Objects.hash(author, books);
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Items(Author author) {
        super();
        this.author = author;
    }
}
