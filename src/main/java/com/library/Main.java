package com.library;

import com.library.entity.Items;
import com.library.management.LibratyMenagment;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        LibratyMenagment libratyMenagment = new LibratyMenagment();
        while (true) {
            System.out.println("1 - add author");
            System.out.println("2 - add book");
            System.out.println("3 - remove book");
            System.out.println("4 - remove author with all his books");
            System.out.println("5 - remove all author's books");
            System.out.println("6 - print all author's books sorted by date");
            System.out.println("7 - print all author's books sorted by pages");
            System.out.println("8 - print all library");
            System.out.println("0 - exit");
            int choise = s.nextInt();
            switch (choise) {
                case 1:
                    libratyMenagment.addAuthor();
                    break;
                case 2:
                    libratyMenagment.addBook();
                    break;
                case 3:
                    libratyMenagment.removeBook();
                    break;
                case 4:
                    libratyMenagment.removeAuthor();
                    break;
                case 5:
                    libratyMenagment.removeAuthorsBooks();
                    break;
                case 6:
                    libratyMenagment.sortBooksByYear();
                    break;
                case 7:
                    libratyMenagment.sortBooksByPages();
                    break;
                case 8:
                    for (Items i : libratyMenagment.getItems())
                        System.out.println(i);
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    break;
            }
        }
    }
}
